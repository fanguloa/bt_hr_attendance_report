# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2018 BroadTech IT Solutions Pvt Ltd 
#    (<http://broadtech-innovations.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU LESSER General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER General Public License for more details.
#
#    You should have received a copy of the GNU LESSER General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from odoo import models, fields, api, _
import xlwt
import io
import base64
from xlwt import easyxf
import datetime
from pytz import timezone
import pytz
from datetime import datetime,timedelta, date
from dateutil.relativedelta import relativedelta
from odoo.exceptions import ValidationError, Warning, UserError


class HrEmployeeAttendanceReport(models.TransientModel):
    _name = "hr.employee.attendance.report"

    from_date = fields.Datetime('From Date',default=datetime.now().strftime('%Y-%m-%d 00:00:00'))
    to_date = fields.Datetime('To Date')
    print_type = fields.Selection([('daily','Daily'),('weekly','Weekly'),('monthly','Monthly')],string="Print By")
    all_employees = fields.Boolean('All Employees',default=True)
    employee_ids = fields.Many2many('hr.employee', 'attendance_employee_rel', 'employee_id', 'attendance_id', string="Employee")
    attendance_report_file = fields.Binary('Employee Attendance Sheet')
    file_name = fields.Char('File Name')
    attendance_report_printed = fields.Boolean('Attendance Report Printed')
    
    @api.onchange('from_date','print_type')
    def onchange_from_date(self):
        if self.print_type:
            start_d, end_d = self.get_dates()
            self.to_date = end_d.strftime('%Y-%m-%d 00:01:01')
    
    @api.multi
    def get_dates(self):
        for wizard in self:
            start_date = datetime.strptime(wizard.from_date,'%Y-%m-%d %H:%M:%S').date()
            if wizard.print_type == 'daily':
                end_date = start_date
            if wizard.print_type == 'weekly':
                end_date = start_date + relativedelta(weeks=1) 
            if wizard.print_type == 'monthly':
                end_date = start_date + relativedelta(months=1,days=-1)  
        return start_date,end_date
    
    @api.multi
    def daterange(self, start_date, end_date):
        for n in range(int ((end_date - start_date).days)+1):
            yield start_date + timedelta(n)

    
    @api.multi
    def generate_excel(self):
        workbook = xlwt.Workbook()
        column_heading_style = easyxf('font:height 200;font:bold True;'"borders: top  thin,bottom  thin,right  thin,left  thin")
        worksheet = workbook.add_sheet('Attendance Report')
        worksheet2 = workbook.add_sheet('Attendance Duration Details')
        emp_list = []
        tz = self.env.user.tz
        if not tz:
            raise UserError(_('Please configure timezone for  %s user!')%(self.env.user.name))
        utc_now = pytz.utc.localize(datetime.utcnow())
        pst_now = utc_now.astimezone(pytz.timezone(tz))
        pst_now1 = datetime.strftime(pst_now.replace(microsecond=0), '%Y-%m-%d %H:%M:%S')
        tz_time= datetime.strptime(pst_now1,'%Y-%m-%d %H:%M:%S')
        utc_time = datetime.now().replace(microsecond=0)
        if utc_time > tz_time:
            time_differnce = utc_time -tz_time
        else:
            time_differnce = tz_time -utc_time
        day_hr =0
        if time_differnce.days:
            day_hr = time_differnce.days*24
            time_differnce = time_differnce+timedelta(hours =int(day_hr))
        tz_diff = datetime.strptime(str(time_differnce),'%H:%M:%S').strftime(' %H:%M:%S')
        h = int(tz_diff.split(':')[0])+day_hr
        m = int(tz_diff.split(':')[1])
        for wizard in self:
            if wizard.print_type:
                start_d, end_d = wizard.get_dates()
                n_start_d = datetime.strptime(str(start_d),'%Y-%m-%d').strftime('%d-%m-%Y')
                n_end_d = datetime.strptime(str(wizard.to_date),'%Y-%m-%d %H:%M:%S').strftime('%d-%m-%Y')
                h_col = 2
                d_col =1
                row = 8
                w2_h_col = 1
                w2_row = 3
                date_array = []
                date_wo_checkout = {}
                sign_in_list = []
                if wizard.to_date != end_d:
                    end_d = datetime.strptime(wizard.to_date,'%Y-%m-%d %H:%M:%S').date()
                
                if start_d != end_d:  
                    for single_date in wizard.daterange(start_d, end_d):
                        s_date = datetime.strptime(str(single_date),'%Y-%m-%d').strftime('%d-%m-%Y')
                        date_array.append(single_date)
                        worksheet.write(6, h_col, _(s_date), column_heading_style)
                        worksheet2.write(2, w2_h_col, _(s_date),easyxf('font:height 200;align: horiz center;font:bold True;'))
                        h_col += 1
                        w2_h_col += 1
                    worksheet.write(6, h_col+1, _('Present'), column_heading_style)
                    worksheet.write(6, h_col+2, _('Absent'), column_heading_style)
                else:
                    worksheet.write(6, 2, _(n_start_d), column_heading_style)
                    date_array.append(start_d)
                    worksheet.write(6, 4, _('Present'), column_heading_style)
                    worksheet.write(6, 5, _('Absent'), column_heading_style)
                    worksheet2.write(2, w2_h_col, _(n_start_d),easyxf( 'font:height 200;align: horiz center;font:bold True;'))
                worksheet.write(6,1, _('Employee Name'), column_heading_style)
                worksheet.col(1).width = 6000
                worksheet2.col(0).width = 6000
                if wizard.print_type == 'daily':
                    heading =  'Employee Attendance - ' +  wizard.print_type + ' : ' +n_start_d
                    worksheet.write_merge(3, 3, 0, 5, heading, easyxf('font:height 210; align: horiz center; font:bold True;'))
                    worksheet2.write_merge(0, 0, 0, 1, 'Attendance Duration', easyxf('font:height 210; align: horiz center; font:bold True;'))
                heading =  'Employee Attendance - '+ wizard.print_type + ' : ' + n_start_d + ' To ' + n_end_d
                if wizard.print_type == 'weekly':
                    worksheet.write_merge(3, 3, 2, 7, heading, easyxf('font:height 210; align: horiz center; font:bold True;' ))
                    worksheet2.write_merge(0, 0, 0, 8, 'Attendance Duration', easyxf('font:height 210; align: horiz center; font:bold True;'))
                if wizard.print_type == 'monthly':
                    worksheet.write_merge(3, 3, 2, 7, heading, easyxf('font:height 210; align: horiz center; font:bold True;')) 
                    worksheet2.write_merge(0, 0, 0, 8, 'Attendance Duration', easyxf('font:height 210; align: horiz center; font:bold True;'))           
                if wizard.all_employees:
                    employee_ids = self.env['hr.employee'].search([])
                    
                else:    
                    employee_ids = wizard.employee_ids
                
                for emp in employee_ids:
                    w2_d_col = 1 
                    p_count = 0
                    a_count = 0
                    worksheet.write(row, 1,emp.name , easyxf('font:height 200;align: horiz right;font:bold True;' "borders: top thin,bottom thin,right thin,left thin"))
                    d_col = 2
                    for date in date_array:
                        date_from = date.strftime('%Y-%m-%d 00:00:00')
                        date_to = date.strftime('%Y-%m-%d 23:59:59')
                        if utc_time > tz_time:
                            date_from = datetime.strptime(date_from,'%Y-%m-%d %H:%M:%S')+timedelta(minutes = m, hours = h)
                            date_to = datetime.strptime(date_to,'%Y-%m-%d %H:%M:%S')+timedelta(minutes = m, hours = h)
                        else:
                            date_from = datetime.strptime(date_from,'%Y-%m-%d %H:%M:%S')-timedelta(minutes = m, hours = h)
                            date_to = datetime.strptime(date_to,'%Y-%m-%d %H:%M:%S')-timedelta(minutes = m, hours = h)
                        emp_attendance = self.env['hr.attendance'].search([('check_in','>=',date_from.strftime('%Y-%m-%d %H:%M:%S')),
                                                                ('check_in','<=',date_to.strftime('%Y-%m-%d %H:%M:%S')),
                                                                       ('employee_id','=',emp.id)],order="check_in asc")
                        
                        if emp_attendance:
                            if emp not in emp_list:
                                emp_list.append(emp)
                        
                        if emp_attendance:
                            worksheet.write(row, d_col, 'P' , easyxf('font:height 200;align: horiz right;font:bold True;' "borders: top thin,bottom thin,right thin,left thin")) 
                            p_count += 1
                        else:
                            worksheet.write(row, d_col, 'A' , easyxf('font:height 200;align: horiz right;font:bold True;' "borders: top thin,bottom thin,right thin,left thin")) 
                            a_count += 1
                        d_col += 1
                       
                        
                    worksheet.write(row, d_col+1, p_count , easyxf('font:height 200;align: horiz left;font:bold True;' "borders: top thin,bottom thin,right thin,left thin"))
                    worksheet.write(row, d_col+2, a_count , easyxf('font:height 200;align: horiz left;font:bold True;'"borders: top thin,bottom thin,right thin,left thin"))
                    row += 1
                
                
                for emp_list in emp_list:
                    w2_d_col = 1
                    worksheet2.write(w2_row, 0,emp_list.name , easyxf('font:height 200;align: horiz right;font:bold True;' ))
                    var_sign = False
                    for date in date_array:
                        date_from = date.strftime('%Y-%m-%d 00:00:00')
                        date_to = date.strftime('%Y-%m-%d 23:59:59')
                        if utc_time > tz_time:
                            date_from = datetime.strptime(date_from,'%Y-%m-%d %H:%M:%S')+timedelta(minutes = m, hours = h)
                            date_to = datetime.strptime(date_to,'%Y-%m-%d %H:%M:%S')+timedelta(minutes = m, hours = h)
                        else:
                            date_from = datetime.strptime(date_from,'%Y-%m-%d %H:%M:%S')-timedelta(minutes = m, hours = h)
                            date_to = datetime.strptime(date_to,'%Y-%m-%d %H:%M:%S')-timedelta(minutes = m, hours = h)
                        var = 0.0
                        emp_date = str(emp_list.id)+'_'+ str(date)
                        emp_attendance1 = self.env['hr.attendance'].search([('check_in','>=',date_from.strftime('%Y-%m-%d %H:%M:%S')),
                                                                ('check_in','<=',date_to.strftime('%Y-%m-%d %H:%M:%S')),
                                                                       ('employee_id','=',emp_list.id)],order="check_in asc")
                        if emp_attendance1:
                            for employee in emp_attendance1:
                                if employee.check_in:
                                    check_in = datetime.strptime(employee.check_in,'%Y-%m-%d %H:%M:%S')
                                if employee.check_out:
                                    check_out = datetime.strptime(employee.check_out,'%Y-%m-%d %H:%M:%S')
                                if employee.check_out:
                                    if emp_date not in date_wo_checkout:
                                        duration = check_out - check_in 
                                        var += float(str(duration).split(':')[0] + '.'+ str(duration).split(':')[1])
                                    else:
                                        sign_in = datetime.strptime(date_wo_checkout[emp_date],'%Y-%m-%d %H:%M:%S')
                                        if date_wo_checkout['check_in'] == True:
                                            duration = check_out - sign_in
                                            date_wo_checkout.update({'check_in': False})
                                        else:
                                            duration = check_out - check_in
                                        var += float(str(duration).split(':')[0] +'.' + str(duration).split(':')[1])
                                else:
                                    if emp_date not in date_wo_checkout:
                                        sign_in_list.append(employee.check_in)
                                        date_wo_checkout.update({emp_date:employee.check_in, 'check_in': True})
                            var = format(var,'.2f')
                        else:
                            var = 0
                        worksheet2.write(w2_row, w2_d_col, var , easyxf('font:height 200;align: horiz center;font:bold True;'))      
                        w2_d_col += 1
                    w2_row += 1  
#             
            else:
                raise ValidationError(_("Please select 'Print By' option for the attendance report. "))
        
            fp = io.BytesIO()
            workbook.save(fp)
            excel_file = base64.encodestring(fp.getvalue())
            wizard.attendance_report_file = excel_file
            wizard.file_name = 'Employee Attendance Report.xls'
            wizard.attendance_report_printed = True
            fp.close()
            return {
                    'view_mode': 'form',
                    'res_id': wizard.id,
                    'res_model': 'hr.employee.attendance.report',
                    'view_type': 'form',
                    'type': 'ir.actions.act_window',
                    'context': self.env.context,
                    'target': 'new',
                       }



 # vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
