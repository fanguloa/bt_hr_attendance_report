# -*- coding: utf-8 -*-
##############################################################################
#
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2018 BroadTech IT Solutions Pvt Ltd 
#    (<http://broadtech-innovations.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU LESSER General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER General Public License for more details.
#
#    You should have received a copy of the GNU LESSER General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'HR Attendance Report XLS',
    'version': '0.1',
    'category': 'Generic Modules/Human Resources',
    'summary': """HR Attendance Report XLS""",
    'license':'LGPL-3',
    'price': 10.00,   
    'currency': 'USD',
    'description': """
    HR Attendance Report
""",
    'author' : 'BroadTech IT Solutions Pvt Ltd',
    'website' : 'http://www.broadtech-innovations.com',
    'depends': ['hr','hr_attendance'],
    'images': ['static/description/banner.jpg'],
    'data': [
        'wizard/hr_employee_attendance_report_view.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True
}



# vim:expandtab:smartindent:tabstop=2:softtabstop=2:shiftwidth=2:
